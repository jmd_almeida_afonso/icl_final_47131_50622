package AST.Logic;

import AST.ASTNode;
import Environment.*;
import Types.IType;
import Types.IValue;
import Types.TBool;
import Types.VBool;

import java.util.List;

public class ASTNot implements ASTNode {

    private ASTNode node;

    public ASTNot(ASTNode node) {
        this.node = node;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        IValue l = node.eval(env, mem);
        if (l instanceof VBool) {
            return new VBool(! ((VBool)l).getT());
        }
        return new VBool(false);
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        node.compile(env, code);
        int size = code.size();
        if(code.get(size - 1).equals("sipush 1")) {
            code.remove(size - 1);
        	code.add("sipush 0");
        }else {
            code.remove(size - 1);
        	code.add("sipush 1");
        }
    }

    @Override
    public IType typeCheck(Environment<IType> env) throws Exception {
        IType lhst = node.typeCheck(env);
        if (lhst.equals(TBool.type)) {
            return TBool.type;
        } else throw new Exception("Illegal type for not");
    }
}
