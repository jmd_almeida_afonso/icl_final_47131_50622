package Types;

import java.util.List;

public class TFun {

    IType returnType;
    List<IType> args;

    public TFun(IType returnType, List<IType> args) {
        this.returnType = returnType;
        this.args = args;
    }

    public IType getReturnType() {
        return returnType;
    }

    public List<IType> getArgs() {
        return args;
    }
}
