package AST;

import Environment.*;
import Types.IType;
import Types.IValue;

import java.util.List;

public interface ASTNode {

    IValue eval(Environment<IValue> env, Memory mem);

    void compile(Environment<IValue> env, List<String> code);

    IType typeCheck(Environment<IType> env) throws Exception;
}
