package AST.Math;

import AST.ASTNode;
import Environment.*;
import Types.IType;
import Types.IValue;
import Types.TInt;
import Types.VInt;

import java.util.List;

public class ASTAdd implements ASTNode {
	ASTNode left;
	ASTNode right;
	
	public ASTAdd(ASTNode _left, ASTNode _right) {
		left = _left;
		right = _right;
	}

	public IValue eval(Environment<IValue> env, Memory mem) {
		IValue l = left.eval(env, mem);
		if (l instanceof VInt) {
			IValue r = right.eval(env, mem);
			if (r instanceof VInt) {
				return new VInt(((VInt) l).getValue() + ((VInt) r).getValue());
			}
		}
		return new VInt(0);
	}

	@Override
	public void compile(Environment<IValue> env, List<String> code) {
		left.compile(env, code);
		right.compile(env, code);
		code.add("iadd");
	}

    @Override
    public IType typeCheck(Environment<IType> env) throws Exception {
		IType lhst = left.typeCheck(env);
		IType rhst = right.typeCheck(env);
		if (lhst.equals(TInt.type) && rhst.equals(TInt.type)) {
			return TInt.type;
		} else throw new Exception("Illegal type for +");
    }
}
