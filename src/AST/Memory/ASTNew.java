package AST.Memory;

import AST.ASTNode;
import Environment.Environment;
import Environment.Memory;
import Environment.MemoryCell;
import Types.IType;
import Types.IValue;
import Types.VCell;

import java.util.List;

public class ASTNew implements ASTNode {

    //private String id;
    private ASTNode node;

    public ASTNew(ASTNode node) {
        //this.id = id;
        this.node = node;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        IValue val = node.eval(env, mem);
        int loc = mem.assoc(new MemoryCell(val));
        VCell vCell = new VCell(loc, val);
        return vCell;
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        /*
    	String ref = Compile.Compiler.generateReferenceFile();

        code.add("new " + ref);
        code.add("dup");
        code.add("invokespecial " + ref + "/<init>()V");
        code.add("dup");
        node.compile(env, code);
        code.add("putfield " + ref + "/v I");
    	*/
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }
}
