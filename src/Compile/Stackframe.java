package Compile;

import java.util.List;

public class Stackframe {

    String name;
    String ant;
    List<String> vars;

    public Stackframe(String name, String ant, List<String> vars) {
        this.name = name;
        this.ant = ant;
        this.vars = vars;
    }

    public List<String> getVars() {
        return vars;
    }

    public String getName() {
        return name;
    }

    public String getAnt() {
        return ant;
    }
}
