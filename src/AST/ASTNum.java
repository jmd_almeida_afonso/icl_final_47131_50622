package AST;

import Environment.*;
import Types.IType;
import Types.IValue;
import Types.TInt;
import Types.VInt;

import java.util.List;

public class ASTNum implements ASTNode {

	int num;

	public ASTNum(int num) {
		this.num = num;
	}

	@Override
	public IValue eval(Environment<IValue> env, Memory mem) {
		return new VInt(num);
	}

	@Override
	public void compile(Environment<IValue> env, List<String> code) {
		//considers numbers different than 0 to be true
			code.add("sipush " + num);
	}

	@Override
	public IType typeCheck(Environment<IType> env) {
		return TInt.type;
	}

}
