package AST;

import Compile.Compiler;
import Compile.Stackframe;
import Environment.Environment;
import Environment.Memory;
import Types.IType;
import Types.IValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ASTLet implements ASTNode {

	private Map<String, ASTNode> id; //ids, inits
	private ASTNode body;
	
	public ASTLet(Map<String, ASTNode> id, ASTNode body) {
		this.id = id;
		this.body = body;
	}

	@Override
	public IValue eval(Environment<IValue> env, Memory mem) {
		Map<String, IValue> lo1 = new HashMap<>();

		for (Entry<String, ASTNode> kv : id.entrySet()) {
			lo1.put(kv.getKey(), kv.getValue().eval(env, mem));
		}

		env = env.beginScope();

		for (Entry<String, IValue> set : lo1.entrySet()) {
			//IValue v = set.getValue();
			env.assoc(set.getKey(), set.getValue());
		}
		
		IValue o2 = body.eval(env, mem);
		env = env.endScope();
		return o2;
	}

	@Override
	public void compile(Environment<IValue> env, List<String> code) {
        //		Entry<String, ASTNode> entry = id.entrySet().stream().findFirst().get();
        //		String var = entry.getKey();
        //		ASTNode init = entry.getValue();

		ArrayList<String> letConstruct = new ArrayList<>();

        Stackframe frame = Compiler.generateFrameFile("f", new ArrayList<>(id.keySet()));

		//create frame for this let block (f0)
		letConstruct.add("new " + frame.getName());
		letConstruct.add("dup");
		letConstruct.add("invokespecial " + frame.getName() + "/<init>()V");
		letConstruct.add("dup");

		//store SL in new frame
		letConstruct.add("aload 4");
		letConstruct.add("putfield " + frame.getName() + "/sl L" + frame.getAnt() + ";");
		//update SL
		letConstruct.add("astore 4");

		//store value of x
        //		letConstruct.add("aload 4");
//		int initEval = init.eval(env);
//		letConstruct.add("sipush " + initEval);

		code.addAll(code.size(), letConstruct);
		letConstruct.clear();

        for (Entry<String, ASTNode> varInit : id.entrySet()) {
            code.add("aload 4");
            varInit.getValue().compile(env, code);
            code.add("putfield " + frame.getName() + "/" + varInit.getKey() + " I");
        }

        //		init.compile(env, code);
        //		code.add("putfield " + frame.getName() + "/" + var + " I");


		//start let body
		body.compile(env, code);

	}

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }
}
