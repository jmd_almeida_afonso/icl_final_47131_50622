package AST;

import Compile.Compiler;
import Environment.*;
import Types.IType;
import Types.IValue;
import Types.VBool;

import java.util.List;

public class ASTWhile implements ASTNode {

    private ASTNode cond;
    private ASTNode body;

    public ASTWhile(ASTNode cond, ASTNode body) {
        this.cond = cond;
        this.body = body;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        IValue eval = cond.eval(env, mem);
        IValue v = null;

        if (eval instanceof VBool) {
            boolean val = ((VBool) eval).getT();
            while(val) {
                v = body.eval(env, mem);
                IValue eval1 = cond.eval(env, mem);
                if (eval1 instanceof VBool) val = ((VBool) eval1).getT();
            }
            return v;
        }
        return new VBool(false);
    }
   
    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        int dowhile = Compiler.getNextLabel();
        int exit = Compiler.getNextLabel();

        code.add("L" + dowhile + ":");
        cond.compile(env, code);
        code.add("ifne L" + exit);
        body.compile(env, code);
        code.add("goto L" + dowhile);
        code.add("L" + exit + ":");
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }
}
