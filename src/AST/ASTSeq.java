package AST;

import Environment.*;
import Types.IType;
import Types.IValue;

import java.util.List;

public class ASTSeq implements ASTNode {

    private ASTNode left;
    private ASTNode right;

    public ASTSeq(ASTNode left, ASTNode right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        left.eval(env, mem);
        return right.eval(env, mem);
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        left.compile(env, code);
        //pop
        right.compile(env, code);
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }
}
