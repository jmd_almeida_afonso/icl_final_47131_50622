package Environment;

import java.util.HashMap;
import java.util.Map;

public class Environment<T> {

	private Map<String, T> vars;
	private Environment<T> parent;

	public Environment(Environment<T> env) {
		vars = new HashMap<>();
		parent = env;
	}
	
	public Environment<T> beginScope() {
		return new Environment<T>(this);
	}
	
	public Environment<T> endScope() {
		return parent;
	}
	
	public void assoc(String id, T value) {
		vars.put(id, value);
	}
	
	public T find(String id) {
		T value = vars.get(id);

		if (value == null) {
			if (parent != null) {
				return parent.find(id);
			} else {
				return null;
			}
		} else {
			return value;
		}
	}
	
}
