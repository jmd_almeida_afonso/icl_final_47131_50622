package Environment;

import java.util.HashMap;
import java.util.Map;

public class Memory {

    private Map<Integer, MemoryCell> cells;

    private int nextLoc = 0; //endereco de memoria

    public Memory() {
        this.cells = new HashMap<Integer, MemoryCell>();
    }

    public int assoc(MemoryCell memoryCell) {
        int loc = nextLoc;
        cells.put(nextLoc++, memoryCell);
        return loc;
    }

    public MemoryCell find(int loc) {
        return cells.get(loc);
    }
}
