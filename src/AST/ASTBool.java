package AST;

import Environment.*;
import Types.IType;
import Types.IValue;
import Types.TBool;
import Types.VBool;

import java.util.List;

public class ASTBool implements ASTNode {

    private boolean bool;

    public ASTBool(boolean bool) {
        this.bool = bool;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        return new VBool(bool);
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        if (bool) code.add("sipush 1");
        else code.add("sipush 0");
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return TBool.type;
    }
}
