/* Generated By:JavaCC: Do not edit this line. ParserConstants.java */

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface ParserConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int Num = 4;
  /** RegularExpression Id. */
  int PLUS = 5;
  /** RegularExpression Id. */
  int MINUS = 6;
  /** RegularExpression Id. */
  int TIMES = 7;
  /** RegularExpression Id. */
  int DIV = 8;
  /** RegularExpression Id. */
  int LPAR = 9;
  /** RegularExpression Id. */
  int RPAR = 10;
  /** RegularExpression Id. */
  int EL = 11;
  /** RegularExpression Id. */
  int LET = 12;
  /** RegularExpression Id. */
  int COM = 13;
  /** RegularExpression Id. */
  int IN = 14;
  /** RegularExpression Id. */
  int END = 15;
  /** RegularExpression Id. */
  int DEQUALS = 16;
  /** RegularExpression Id. */
  int NOT = 17;
  /** RegularExpression Id. */
  int REF = 18;
  /** RegularExpression Id. */
  int GREATER = 19;
  /** RegularExpression Id. */
  int GREATERE = 20;
  /** RegularExpression Id. */
  int LESS = 21;
  /** RegularExpression Id. */
  int LESSE = 22;
  /** RegularExpression Id. */
  int AND = 23;
  /** RegularExpression Id. */
  int OR = 24;
  /** RegularExpression Id. */
  int NEW = 25;
  /** RegularExpression Id. */
  int ASSOC = 26;
  /** RegularExpression Id. */
  int EQUALS = 27;
  /** RegularExpression Id. */
  int COMMA = 28;
  /** RegularExpression Id. */
  int TRUE = 29;
  /** RegularExpression Id. */
  int FALSE = 30;
  /** RegularExpression Id. */
  int FUN = 31;
  /** RegularExpression Id. */
  int FUNB = 32;
  /** RegularExpression Id. */
  int IF = 33;
  /** RegularExpression Id. */
  int THEN = 34;
  /** RegularExpression Id. */
  int ELSE = 35;
  /** RegularExpression Id. */
  int WHILE = 36;
  /** RegularExpression Id. */
  int DO = 37;
  /** RegularExpression Id. */
  int PRINT = 38;
  /** RegularExpression Id. */
  int Id = 39;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\t\"",
    "\"\\r\"",
    "<Num>",
    "\"+\"",
    "\"-\"",
    "\"*\"",
    "\"/\"",
    "\"(\"",
    "\")\"",
    "\"\\n\"",
    "\"let\"",
    "\";\"",
    "\"in\"",
    "\"end\"",
    "\"==\"",
    "\"not\"",
    "\"!\"",
    "\">\"",
    "\">=\"",
    "\"<\"",
    "\"<=\"",
    "\"and\"",
    "\"or\"",
    "\"new\"",
    "\":=\"",
    "\"=\"",
    "\",\"",
    "\"true\"",
    "\"false\"",
    "\"fun\"",
    "\"->\"",
    "\"if\"",
    "\"then\"",
    "\"else\"",
    "\"while\"",
    "\"do\"",
    "\"println\"",
    "<Id>",
  };

}
