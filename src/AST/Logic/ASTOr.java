package AST.Logic;

import AST.ASTNode;
import Environment.*;
import Types.*;

import java.util.List;

public class ASTOr implements ASTNode {

    private ASTNode left;
    private ASTNode right;

    public ASTOr(ASTNode left, ASTNode right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        IValue l = left.eval(env, mem);
        if (l instanceof VBool) {
            IValue r = right.eval(env, mem);
            if (r instanceof VBool) {
                return new VBool(((VBool) l).getT() || ((VBool) r).getT());
            }
        }
        return new VBool(false);
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        left.compile(env, code);
        right.compile(env, code);
        code.add("ior");
    }

    @Override
    public IType typeCheck(Environment<IType> env) throws Exception {
        IType lhst = left.typeCheck(env);
        IType rhst = right.typeCheck(env);
        if ((lhst.equals(TBool.type) && rhst.equals(TBool.type))) {
            return TBool.type;
        } else throw new Exception("Illegal type for or");
    }
}
