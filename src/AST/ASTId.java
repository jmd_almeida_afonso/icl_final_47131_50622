package AST;

import Compile.Compiler;
import Compile.Stackframe;
import Environment.*;
import Types.IType;
import Types.IValue;

import java.util.List;

public class ASTId implements ASTNode {

	private String id;
	
	public ASTId(String id) {
		this.id = id;
	}

	@Override
	public IValue eval(Environment<IValue> env, Memory mem) {
		return env.find(id);
	}

	@Override
	public void compile(Environment<IValue> env, List<String> code) {
//		; fetch x coordinates (1,0)
//		aload 4
//		getfield f1/sl Lf0;
//		getfield f0/x0 I

		if (Compiler.lastFrame == null) {

			code.add("sipush 0");
		} else {

			code.add("aload 4");

			Stackframe sf = new Stackframe(Compiler.lastFrame.getName(), Compiler.lastFrame.getAnt(),
					Compiler.lastFrame.getVars());

			boolean setDataflag = false;
			if ((code.get(code.size() - 1).equals(code.get(code.size() - 2))) && !sf.getName().equals("f0")) {
				setDataflag = true;
			}

			do {
				// just missing an adjust in here for case let x=c in x end
				if (sf.getVars().contains(id) && code.contains("putfield " + sf.getName() + "/" + id + " I")
						&& !setDataflag) {

					code.add("getfield " + sf.getName() + "/" + id + " I");
					break;
				} else {
					code.add("getfield " + sf.getName() + "/sl L" + sf.getAnt() + ";");
					sf = Compiler.stack.get(sf.getAnt());
					setDataflag = false;
				}

			} while (sf != null );
		}
	}

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }

}
