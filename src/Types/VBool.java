package Types;

public class VBool implements IValue {

    boolean t;

    public VBool(boolean t) {
        this.t = t;
    }

    public boolean getT() {
        return t;
    }

    public void setT(boolean t) {
        this.t = t;
    }

    @Override
    public String show() {
        return "" + t;
    }
}
