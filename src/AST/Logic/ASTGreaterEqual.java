package AST.Logic;

import AST.ASTNode;
import Compile.Compiler;
import Environment.*;
import Types.*;

import java.util.List;

public class ASTGreaterEqual implements ASTNode {

    private ASTNode left;
    private ASTNode right;

    public ASTGreaterEqual(ASTNode left, ASTNode right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        IValue l = left.eval(env, mem);
        if (l instanceof VInt) {
            IValue r = right.eval(env, mem);
            if (r instanceof VInt) {
                return new VBool(((VInt) l).getValue() >= ((VInt) r).getValue());
            }
        }
        return new VBool(false);
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
    	left.compile(env, code);
        right.compile(env, code);
        code.add("isub");
        
        int labelTrue = Compiler.getNextLabel();
        int labelFalse = Compiler.getNextLabel();
        
        code.add("if_icmpge L" + labelTrue);
        code.add("sipush 0");
        code.add("goto L" + labelFalse);
        code.add("L" + labelTrue + ":");
        code.add("sipush 1");
        code.add("L" + labelFalse + ":");
    }

    @Override
    public IType typeCheck(Environment<IType> env) throws Exception {
        IType lhst = left.typeCheck(env);
        IType rhst = right.typeCheck(env);
        if (lhst.equals(TInt.type) && rhst.equals(TInt.type)) {
            return TBool.type;
        } else throw new Exception("Illegal type for gtq");
    }
}
