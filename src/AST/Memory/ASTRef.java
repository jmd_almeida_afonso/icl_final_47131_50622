package AST.Memory;

import AST.ASTNode;
import Environment.*;
import Types.IType;
import Types.IValue;
import Types.VCell;

import java.util.List;

public class ASTRef implements ASTNode {

    //private String id;
    private ASTNode node;

    /*public ASTRef(String id) {
        this.id = id;
    }*/

    public ASTRef(ASTNode node) {
        this.node = node;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        //IValue value = env.find(id);
        IValue value = node.eval(env, mem);

        if (value instanceof VCell) {
            int loc = ((VCell) value).getLoc();
            return mem.find(loc).getCell();
        }

        return null;
    }

    //TODO: generalize
    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        /*
    	node.compile(env, code);
        code.add("checkcast ref_0");
        code.add("getfield ref_0/v I");
    	*/
    }

    @Override
    public IType typeCheck(Environment<IType> env) throws Exception {
        return null;
    }
}
