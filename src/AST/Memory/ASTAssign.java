package AST.Memory;

import AST.ASTNode;
import Environment.Environment;
import Environment.Memory;
import Types.IType;
import Types.IValue;
import Types.VCell;
import Types.VInt;

import java.util.List;

public class ASTAssign implements ASTNode {

    private ASTNode id;
    private ASTNode right;

    public ASTAssign(ASTNode id, ASTNode right) {
        this.id = id;
        this.right = right;
    }

    //check
    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        IValue val = right.eval(env, mem);
        //IValue cell = env.find(id);
        IValue cell = id.eval(env, mem);

        if (cell instanceof VCell) {
            int loc = ((VCell) cell).getLoc();
            mem.find(loc).setCell(val);
            return mem.find(loc).getCell();
        } else
            return new VInt(0);
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
        /*
    	id.compile(env, code);
        code.add("checkcast " + ""); //todo get ref
        right.compile(env, code);
        code.add("putfield" + "" + "/v I"); //todo get ref
		*/
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }
}
