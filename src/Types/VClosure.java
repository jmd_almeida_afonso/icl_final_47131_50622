package Types;

import AST.ASTNode;
import Environment.*;

import java.util.List;

public class VClosure implements IValue {

    private Environment<IValue> env;
    private ASTNode body;
    private List<String> argList;

    public VClosure(Environment<IValue> env, ASTNode body, List<String> argList) {
        this.env = env;
        this.body = body;
        this.argList = argList;
    }


    public Environment<IValue> getEnv() {
        return env;
    }

    public ASTNode getBody() {
        return body;
    }

    public List<String> getArgList() {
        return argList;
    }

    @Override
    public String show() {
        return "VClosure{argList=" + String.join(",", argList) + '}';
    }
}
