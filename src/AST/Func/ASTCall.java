package AST.Func;

import AST.ASTNode;
import Environment.*;
import Types.IType;
import Types.IValue;
import Types.VClosure;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ASTCall implements ASTNode {

    private ASTNode id;
    private List<ASTNode> args;

    public ASTCall(ASTNode id, List<ASTNode> args) {
        this.id = id;
        this.args = args;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) { //todo: might need to be redone
        IValue func = id.eval(env, mem);
        if (! (func instanceof VClosure)) {
            System.out.println("Not a function");
            return null;
        }
        if (args.size() != ((VClosure) func).getArgList().size()) {
            System.out.println("Incorrect number of arguments");
            return null;
        }

        Map<String, IValue> evald = new HashMap<>(args.size());
        for (int i = 0; i < ((VClosure) func).getArgList().size(); i++) {
            ASTNode arg = args.get(i);
            String argName = ((VClosure) func).getArgList().get(i);
            evald.put(argName, arg.eval(((VClosure) func).getEnv(), mem));
        }

        env = env.beginScope();

        for (Map.Entry<String, IValue> entry : evald.entrySet()) {
            env.assoc(entry.getKey(), entry.getValue());
        }

        IValue rValue = ((VClosure) func).getBody().eval(env, mem);
        env = env.endScope();
        return rValue;
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {

    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }
}
