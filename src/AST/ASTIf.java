package AST;

import Compile.Compiler;
import Environment.*;
import Types.IType;
import Types.IValue;
import Types.VBool;

import java.util.List;

public class ASTIf implements ASTNode {

    private ASTNode cond;
    private ASTNode truePart;
    private ASTNode falsePart;

    public ASTIf(ASTNode cond, ASTNode truePart, ASTNode falsePart) {
        this.cond = cond;
        this.truePart = truePart;
        this.falsePart = falsePart;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        IValue eval = cond.eval(env, mem);
        if (eval instanceof VBool) {
            if (((VBool) eval).getT()) {
                return truePart.eval(env, mem);
            } else return falsePart.eval(env, mem);
        }
        return new VBool(false);
    }
    
    @Override
    public void compile(Environment<IValue> env, List<String> code) {
    	int labelTrue = Compiler.getNextLabel();
        int labelFalse = Compiler.getNextLabel();

        cond.compile(env, code);
        code.add("ifne  L" + labelTrue);
        falsePart.compile(env, code);
        code.add("goto L" + labelFalse);
        code.add("L" + labelTrue + ":");
        truePart.compile(env, code);
        
        code.add("L" + labelFalse + ":");
        
        
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }
}
