package Compile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Compiler {

	private static int nframes = 0;
    private static int frames = 0;
    private static String ant = "java/lang/Object";

    public static Map<String, Stackframe> stack = new LinkedHashMap<>();
    public static Stackframe lastFrame = null;
    public static boolean NewFile = true;
    
    private static int cmpLabel = 1;

    public Compiler() {

    }

    public void compile(List<String> code) throws FileNotFoundException {
        File file = new File("comp.j");
        PrintWriter pw = new PrintWriter(file);

        if(NewFile) {
        	code.addAll(0, writeHeader());	
        	NewFile = false;
        	nframes++;
        	frames = 0;
        }else {
        	nframes++;
        	frames = 0;
        	lastFrame = null;
        	stack = new LinkedHashMap<>();
        	int indexR = code.lastIndexOf("return");
        	code.remove(indexR);
        	int indexE = code.lastIndexOf(".end method");
        	code.remove(indexE);
        	
        	int indexL = code.lastIndexOf("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        	code.add(indexL + 1, "astore 4");
        	code.add(indexL + 1, "aconst_null");
        	code.add(indexL + 1, "getstatic java/lang/System/out Ljava/io/PrintStream;");
        }
     
        code.addAll(code.size(), writeFooter());

        code.forEach(i -> pw.println(i));
        pw.flush();
        pw.close();
    }
    
    public static int getNextLabel() {
        return cmpLabel++;
    }
    
    private static int refs = 0;

    public static String generateReferenceFile() {
            int f = refs++;
            File file = new File("ref_" + f + ".j");
            ArrayList<String> lines = new ArrayList<>();

            lines.add(".class " + "ref_" + f);
            lines.add(".super java/lang/Object");
            lines.add(".field public sl L" + ant + ";");

            lines.add(".field public v" + " I"); //todo: check type

            lines.add(".method public <init>()V");
            lines.add("aload_0");
            lines.add("invokenonvirtual java/lang/Object/<init>()V");
            lines.add("return");

            lines.add(".end method");

            try {
                PrintWriter pw = new PrintWriter(file);

                lines.forEach(pw::println);

                pw.flush();
                pw.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return "ref_" + f;
        }

    public static Stackframe generateFrameFile(String className, List<String> vars) {
        int f = frames++;
        File file = new File(className + "_" + nframes + "_" + f + ".j");
        ArrayList<String> lines = new ArrayList<>();

        lines.add(".class " + className + "_" + nframes + "_" + f );
        lines.add(".super java/lang/Object");
        lines.add(".field public sl L" + ant + ";");

        for (String var : vars) {
            lines.add(".field public " + var + " I");
        }

        lines.add(".method public <init>()V");
        lines.add("aload_0");
        lines.add("invokenonvirtual java/lang/Object/<init>()V");
        lines.add("return");

        lines.add(".end method");

        try {
            PrintWriter pw = new PrintWriter(file);

            lines.forEach(pw::println);

            pw.flush();
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Stackframe stackframe = new Stackframe(
        		className + "_" + nframes + "_" + f,
            ant,
            vars);
        ant = className + "_" + nframes + "_" + f ;

        stack.put(stackframe.getName(), stackframe);
        lastFrame = stackframe;
        return stackframe;
    }

    private List<String> writeHeader() {
        ArrayList<String> header = new ArrayList<>();

        header.add(".class public Comp");
        header.add(".super java/lang/Object");
        header.add("");
        header.add(".method public <init>()V");
        header.add("aload_0");
        header.add("invokenonvirtual java/lang/Object/<init>()V");
        header.add("return");
        header.add(".end method");
        header.add("");
        header.add(".method public static main([Ljava/lang/String;)V");
        header.add(".limit locals 10");
        header.add(".limit stack 256");
        header.add("getstatic java/lang/System/out Ljava/io/PrintStream;");

        //let consts
        //consts
        header.add("aconst_null");
        header.add("astore 4");

        return header;
    }

    private List<String> writeFooter() {
        ArrayList<String> footer = new ArrayList<>();

        footer.add("invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
        footer.add("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        footer.add("return");
        footer.add(".end method");

        return footer;
    }
}
