package Types;

public class VCell implements IValue {
    IValue v;
    int loc;

    public VCell(int loc, IValue v) {
        this.loc = loc;
        this.v = v;
    }

//    public VCell(IValue v) {
//        this.v = v;
//    }

    public int getLoc() {
        return loc;
    }

    public IValue getV() {
        return v;
    }

    public void setV(IValue v) {
        this.v = v;
    }

    @Override
    public String show() {
        return v.show();
    }
}

