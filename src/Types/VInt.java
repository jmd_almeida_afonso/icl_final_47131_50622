package Types;

public class VInt implements IValue {

    int v;

    public VInt(int v) {
        this.v = v;
    }

    public int getValue() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    @Override
    public String show() {
        return "" + v;
    }
}
