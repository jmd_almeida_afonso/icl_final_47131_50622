package AST.Func;

import AST.ASTNode;
import Environment.*;
import Types.IType;
import Types.IValue;
import Types.VClosure;

import java.util.List;

public class ASTFunc implements ASTNode {

    private List<String> argList;
    private ASTNode body;
    private Environment<IValue> env;

    public ASTFunc(List<String> argList, ASTNode body, Environment<IValue> env) {
        this.argList = argList;
        this.body = body;
        this.env = env;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        VClosure vClosure = new VClosure(this.env, body, argList);
        return vClosure;
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }


}
