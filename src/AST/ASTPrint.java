package AST;

import Environment.*;
import Types.IType;
import Types.IValue;

import java.util.List;

public class ASTPrint implements ASTNode { //todo

    ASTNode node;

    public ASTPrint(ASTNode node) {
        this.node = node;
    }

    @Override
    public IValue eval(Environment<IValue> env, Memory mem) {
        System.out.println(node.eval(env, mem).show());
        return node.eval(env, mem);
    }

    @Override
    public void compile(Environment<IValue> env, List<String> code) {
    	node.compile(env, code);
    }

    @Override
    public IType typeCheck(Environment<IType> env) {
        return null;
    }

}
