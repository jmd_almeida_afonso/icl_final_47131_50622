package Environment;

import Types.IValue;

public class MemoryCell {

    private IValue cell;

    public MemoryCell(IValue cell) {
        this.cell = cell;
    }

    public IValue getCell() {
        return cell;
    }

    public void setCell(IValue cell) {
        this.cell = cell;
    }
}
